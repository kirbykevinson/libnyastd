.POSIX:

CP = cp -f
MKDIR = mkdir -p
RM = rm -f

HEADER = nyastd.h

OBJECTS = pathtype.o trashthrasher.o stdstds.o

BIN = libnyastd.a

PREFIX = /usr/local

all: $(OBJECTS)
	$(AR) -rcs $(BIN) $(OBJECTS)

pathtype.o: pathtype.c $(HEADER)
	$(CC) $(CFLAGS) -c pathtype.c

trashthrasher.o: trashthrasher.c $(HEADER)
	$(CC) $(CFLAGS) -c trashthrasher.c

stdstds.o: stdstds.c $(HEADER)
	$(CC) $(CFLAGS) -c stdstds.c

clean:
	$(RM) $(OBJECTS) $(BIN)

install: all
	$(MKDIR) $(PREFIX)/lib
	$(MKDIR) $(PREFIX)/include
	
	$(CP) $(BIN) $(PREFIX)/lib
	$(CP) $(HEADER) $(PREFIX)/include

uninstall:
	$(RM) $(PREFIX)/bin/$(BIN)
	$(RM) $(PREFIX)/include/$(HEADER)
