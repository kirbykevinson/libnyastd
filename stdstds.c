#include "nyastd.h"

char *nyastd_name = NULL;

void nyastd_init(int *argc, char ***argv) {
	nyastd_name = basename(*argv[0]);
	
	
}

void nyastd_die(struct NyastdObject *last_words) {
	char *real_last_words = NULL;
	
	if (last_words->type == NYASTD_OBJECT_STRING) {
		real_last_words = last_words->value.as_string.value;
	} else {
		real_last_words = "(malformed error message)";
	}
	
	printf("%s: error: %s\n", nyastd_name, real_last_words);
	exit(EXIT_FAILURE);
}
void nyastd_self_murder(char *last_words) {
	printf("%s: fatal error: %s\n", nyastd_name, last_words);
	exit(EXIT_FAILURE);
}
