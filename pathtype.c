#include "nyastd.h"

struct NyastdObject *nyastd_create_nothing(void) {
	struct NyastdObject *object;
	
	if ((object = malloc(sizeof(struct NyastdObject))) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	object->type = NYASTD_OBJECT_NOTHING;
	
	return object;
}
struct NyastdObject *nyastd_create_bool(bool value) {
	struct NyastdObject *object;
	
	if ((object = malloc(sizeof(struct NyastdObject))) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	object->type = NYASTD_OBJECT_BOOL;
	
	object->value.as_bool = value;
	
	return object;
}
struct NyastdObject *nyastd_create_int(int64_t value) {
	struct NyastdObject *object;
	
	if ((object = malloc(sizeof(struct NyastdObject))) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	object->type = NYASTD_OBJECT_INT;
	
	object->value.as_int = value;
	
	return object;
}
struct NyastdObject *nyastd_create_float(double value) {
	struct NyastdObject *object;
	
	if ((object = malloc(sizeof(struct NyastdObject))) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	object->type = NYASTD_OBJECT_FLOAT;
	
	object->value.as_int = value;
	
	return object;
}
struct NyastdObject *nyastd_create_string(
	size_t length,
	
	char *value
) {
	struct NyastdObject *object;
	
	if ((object = malloc(sizeof(struct NyastdObject))) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	object->type = NYASTD_OBJECT_STRING;
	
	object->value.as_string = (struct NyastdString) {
		.length = length,
		
		.value = value
	};
	
	return object;
}
struct NyastdObject *nyastd_create_function(
	NyastdFunction *value
) {
	struct NyastdObject *object;
	
	if ((object = malloc(sizeof(struct NyastdObject))) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	object->type = NYASTD_OBJECT_FUNCTION;
	
	object->value.as_function = value;
	
	return object;
}
struct NyastdObject *nyastd_create_list(size_t length, ...) {
	struct NyastdObject *object;
	
	va_list args;
	
	if ((object = malloc(sizeof(struct NyastdObject))) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	object->type = NYASTD_OBJECT_LIST;
	
	object->value.as_list = (struct NyastdList) {
		.length = length,
	};
	if ((object->value.as_list.values =
		malloc(length * sizeof(struct NyastdObject))
	) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	va_start(args, length);
	
	for (size_t i = 0; i < length; i++) {
		object->value.as_list.values[i] = va_arg(args,
			struct NyastdObject *
		);
	}
	
	va_end(args);
	
	return object;
}
struct NyastdObject *nyastd_create_table(void) {
	struct NyastdObject *object;
	
	if ((object = malloc(sizeof(struct NyastdObject))) == NULL) {
		nyastd_self_murder(strerror(errno));
	}
	
	object->type = NYASTD_OBJECT_TABLE;
	
	object->value.as_table = (struct NyastdTable) {
		.length = 0,
		
		.items = NULL
	};
	
	return object;
}

struct NyastdObject *nyastd_call(
	struct NyastdObject *callee,
	
	struct NyastdObject *args,
	
	bool called_safely
) {
	if (callee->type == NYASTD_OBJECT_FUNCTION) {
		return callee->value.as_function(args, called_safely);
	} else {
		return callee;
	}
}
