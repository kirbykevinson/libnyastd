#ifndef NYASTD_H
#define NYASTD_H

#define _XOPEN_SOURCE 700

#include <errno.h>
#include <inttypes.h>
#include <libgen.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NYASTD_MAJOR_VERSION 0
#define NYASTD_MINOR_VERSION 0
#define NYASTD_PATCH_VERSION 1

#define NYASTD_CHECK_VERSION(major, minor) (\
	(major) == (NYASTD_MAJOR_VERSION) &&\
	(minor) <= (NYASTD_MINOR_VERSION)\
)

enum NyastdObjectType {
	NYASTD_OBJECT_NOTHING,
	NYASTD_OBJECT_BOOL,
	NYASTD_OBJECT_INT,
	NYASTD_OBJECT_FLOAT,
	NYASTD_OBJECT_STRING,
	NYASTD_OBJECT_FUNCTION,
	NYASTD_OBJECT_LIST,
	NYASTD_OBJECT_TABLE
};

struct NyastdString {
	size_t length; // Excluding null terminator
	
	char *value; // Null-terminated
};

struct NyastdList {
	size_t length;
	
	struct NyastdObject **values;
};

struct NyastdTableItem {
	struct NyastdString *key;
	int64_t key_hash;
	
	struct NyastdObject *value;
};

struct NyastdTable {
	size_t length;
	
	struct NyastdTableItem *items;
};

typedef struct NyastdObject
	*(NyastdFunction)(struct NyastdObject *, bool);

union NyastdObjectValue {
	bool as_bool;
	int64_t as_int;
	double as_float;
	struct NyastdString as_string;
	NyastdFunction *as_function;
	struct NyastdList as_list;
	struct NyastdTable as_table;
};

struct NyastdObject {
	enum NyastdObjectType type;
	
	union NyastdObjectValue value;
};

struct NyastdObject *nyastd_create_nothing(void);
struct NyastdObject *nyastd_create_bool(bool);
struct NyastdObject *nyastd_create_int(int64_t);
struct NyastdObject *nyastd_create_float(double);
struct NyastdObject *nyastd_create_string(size_t, char *);
struct NyastdObject *nyastd_create_function(NyastdFunction *);
struct NyastdObject *nyastd_create_list(size_t, ...);
struct NyastdObject *nyastd_create_table(void);

struct NyastdObject *nyastd_call(
	struct NyastdObject *,
	
	struct NyastdObject *,
	
	bool
);

int64_t nyastd_hash(struct NyastdString);

struct NyastdObject *nyastd_get(
	struct NyastdObject *,
	
	char *,
	...
);
struct NyastdObject *nyastd_set(
	struct NyastdObject *,
	
	struct NyastdObject *,
	
	char *,
	...
);

void nyastd_thrash_trash(void);

extern char *nyastd_name;

void nyastd_init(int *, char ***);

#define NYASTD_DIE(last_words)\
	if (called_safely) {\
		return last_words;\
	}\
	nyastd_die(last_words);

void nyastd_die(struct NyastdObject *);
void nyastd_self_murder(char *);

#endif
